package MatrixOperations;

import java.util.Scanner;

public class MatrixOperations
{

    public static void main(String [] args) {
        int[][] matrix1 = new int[5][5];
        int[][] matrix2 = new int[5][5];
        int[][] resultmatrix = new int[5][5];

        int i, j, k, rowcount, columncount;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the rows :");
        rowcount = sc.nextInt();

        System.out.println("Enter the columns:");
        columncount = sc.nextInt();

        System.out.println("Insert the values of matrix1:");
        for (i = 0; i < rowcount; i++)
            for (j = 0; j < columncount; j++)
                matrix1[i][j] = sc.nextInt();

        System.out.println("Insert the values of matrix2:");
        for (i = 0; i < rowcount; i++)
            for (j = 0; j < columncount; j++)
                matrix2[i][j] = sc.nextInt();

        //addition For i=0 to A.row
        //    For j=0 to A.column
        //        Cij =  Aij + Bij

        for (i = 0; i < rowcount; i++)
            for (j = 0; j < columncount; j++)
                resultmatrix[i][j] = matrix1[i][j] + matrix2[i][j];

        System.out.println("Result matrix on addition is :");
        for (i = 0; i < rowcount; i++) {
            for (j = 0; j < columncount; j++) {
                System.out.print(resultmatrix[i][j] + " ");
            }
            System.out.println();
        }

        //subtraction For i=0 to A.row
        //    For j=0 to A.column
        //        Cij =  Aij - Bij

        for (i = 0; i < rowcount; i++)
            for (j = 0; j < columncount; j++)
                resultmatrix[i][j] = matrix1[i][j] - matrix2[i][j];

        System.out.println("Result matrix on subtraction is :");
        for (i = 0; i < rowcount; i++) {
            for (j = 0; j < columncount; j++) {
                System.out.print(resultmatrix[i][j] + " ");
            }
            System.out.println();
        }



        //  For i=1 to A.row  columnof1 and rowof2 is same for multiply
        //For j=1 to B.column
        //For k=1 to B.row
        //  Cij =  Σ  ( Aik * Bkj)

        System.out.println("enter the columns of matrix1 or rows of matrix2 :");
        int count= sc.nextInt();
        int[][] multresultmatrix = new int[5][5];

        for (i = 0; i < rowcount; i++)
            for (j = 0; j < columncount; j++)
                for (k = 0; k < count; k++)
                    multresultmatrix[i][j] = multresultmatrix[i][j] + (matrix1[i][k] * matrix2[k][j]);

        System.out.println("Result matrix on multiplication is :");
        for (i = 0; i < rowcount; i++){
            for (j = 0; j < columncount; j++) {
                System.out.print(multresultmatrix[i][j] + " ");
            }
                System.out.println();
            }







    }






}
