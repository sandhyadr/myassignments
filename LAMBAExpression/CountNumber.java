package LAMBAExpression;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


//Count number of 2’s in the list
  //      Original List: [1,2,2,4,2,5]
    //    Count of 2’s:  3
      //  Hint: Use Count operator
public class CountNumber {

    public static void main(String[] args) {
        List<Integer> intlist = Arrays.asList(1,2,2,4,2,5);

        int count= (int) intlist.stream().filter(i -> i == 2).count();
        System.out.println(count);

    }

    }
