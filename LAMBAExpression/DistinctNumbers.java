package LAMBAExpression;
//Create List of square of all distinct numbers
//Original List: [9, 10, 3, 4, 7, 3, 4]
//Square Without duplicates: [81, 100, 9, 16, 49]
//Hint: Use distinct operator

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DistinctNumbers {

    public static void main(String[] args) {
        List<Integer> intlist = Arrays.asList(9, 10, 3, 4, 7, 3, 4);

        System.out.println(intlist.stream().distinct().map(i  -> i * i).collect(Collectors.toList()));


    }

    }
