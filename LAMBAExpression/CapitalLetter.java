package LAMBAExpression;


//.Convert String to Uppercase and join them using coma
//Original List: [“USA”, “Japan”, “France”, “Germany”, “India”, “U.K.“,”Canada”]
//Output: USA, JAPAN, FRANCE, GERMANY, INDIA, U.K., CANADA


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CapitalLetter {
    public static void main(String[] args)
    {
        List<String> list1= Arrays.asList("USA", "Japan", "France", "Germany", "India", "U.K.","Canada");

        System.out.println(list1.stream().map(i -> i.toUpperCase()).collect(Collectors.joining(",")));

    }

}
