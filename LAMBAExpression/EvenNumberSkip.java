package LAMBAExpression;
//skip first 4  even numbers from the list
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EvenNumberSkip {


    public static void main(String[] args) {
        List<Integer> intlist = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
        Stream<Integer> stream = intlist.stream();

        System.out.println( intlist.stream().filter(i -> i % 2 == 0).skip(4).collect(Collectors.toList()) ) ;

    }
    }
