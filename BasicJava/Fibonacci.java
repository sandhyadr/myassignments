package BasicJava;

//find the fibonacci series 0 1 1 2 3 5 8 13 21 34



public class Fibonacci {

    public static void main(String[] args) {
        int seriesLength = 10;
        int[] fSeries = new int[10];
        fSeries[0] = 0;
        fSeries[1] = 1;
        for (int i = 2; i < seriesLength; i++)
        {
            fSeries[i] = fSeries[i-1]+fSeries[i-2];
        }

        System.out.println("Fibonacci series is :" );
        for (int i = 0 ; i < seriesLength; i++)
        {
             System.out.println(fSeries[i] +" ");
        }
    }
}

